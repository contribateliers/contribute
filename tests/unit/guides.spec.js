
import { mount, config } from '@vue/test-utils'
import '@/setup'
import {getTasks} from '@/data'
import Guide from '@/views/Guide'

const Fake = {
  $pgettext: function(a, b) { return b },
  $language: {
    current: 'en_US'
  },
}
const guides = getTasks(Fake)
config.silent = false


describe("Guide", () => {
  guides.filter((e) => {
    return e.steps && e.steps.length > 0
  }).forEach(g => {
    // render the component
    it(`renders guide ${g.name}`, () => {
      const wrapper = mount(Guide, {
        propsData: { slug: g.slug },
        mocks: Fake,
        stubs: ['router-link'],
      })
      expect(wrapper.text()).toContain(g.name)
      expect(wrapper.text()).toContain(g.summary)
      g.steps.forEach((s) => {
        expect(wrapper.text()).toContain(s.title)
        s.content.forEach(r => {
          expect(wrapper.text()).toContain(r)
        })
      })
    })
  })
});
