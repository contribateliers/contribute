/* eslint-disable */
module.exports = {
  "locales": [
    {
      "code": "ar",
      "label": "العربية"
    },
    {
      "code": "ca",
      "label": "Català"
    },
    {
      "code": "de",
      "label": "Deutsch"
    },
    {
      "code": "en_US",
      "label": "English (United-States)"
    },
    { 
      "code": "en_GB",
      "label": "English (UK)"
    },
    {
      "code": "es",
      "label": "Español"
    },
    {
      "code": "fr_FR",
      "label": "Français"
    },
    {
      "code": "gl",
      "label": "Galego"
    },
    {
      "code": "it",
      "label": "Italiano"
    },
    {
      "code": "nb_NO",
      "label": "Norsk bokmål"
    },
    {
      "code": "nl",
      "label": "Nederlands"
    },
    {
      "code": "pl",
      "label": "Polszczyzna"
    },
    {
      "code": "pt_BR",
      "label": "Português (Brasil)"
    }
  ]
}
