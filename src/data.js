export function getTasks (v) {
  return [
    {
      name: v.$pgettext('Task name', "Do some translations of Framasoft's projects"),
      duration: 15,
      icon: "language",
      tags: [ v.$pgettext('Task tag', 'Translation') ],
      summary: v.$pgettext("Task summary", 'Help translate some projects into other languages.'),
      slug: 'translate-framasoft',
      skills: [
        {
          name: v.$pgettext("Skill name", 'Comprehend English'),
          summary: v.$pgettext("Skill summary", "You will often need to understand written English to translate to another language.")
        },
        {
          name: v.$pgettext("Skill name", 'Be fluent in another language'),
          summary: v.$pgettext("Skill summary", "Good, you meet the requirements of translating into that language.")
        },
      ],
      steps: [
        {
          title: v.$pgettext("Guide step title", 'Create your Contributor Account'),
          content: [v.$pgettext("Guide step content", "You'll need an account on the platform we use to translate projects."),],
          links: [
            {
              icon: 'language',
              text: v.$pgettext("Text for link in guide step", 'Create your Weblate account'),
              to: {name: 'guide', params: {slug: 'weblate-account', locale: v.$language.current}},
            }
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Choose the project you want to translate'),
          content: [v.$pgettext("Guide step content", "Every project can be made up of several subprojects, also known as components in Weblate's terminology. Pick your favourite, or \"Contribateliers\" to translate this website."),],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'List of translatable projects on Weblate'),
              url: 'https://weblate.framasoft.org/projects/'
            }
          ],
        },
        {
          title: v.$pgettext("Guide step title", 'Understand what the project is about'),
          content: [v.$pgettext("Guide step content", "In order to translate appropriately a project, it is better to understand what's it about."),],
        },
        {
          title: v.$pgettext("Guide step title", "Review the eventual translation guidelines"),
          content: [v.$pgettext("Guide step content", "Translator guidelines are often available to ensure consistency across languages and components. Take a moment to read this document if available."),],
        },
        {
          title: v.$pgettext("Guide step title", 'Choose the target language'),
          content: [v.$pgettext("Guide step content", "Each component is translated in many languages. Pick a language your are fluent in."),],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'List of already available languages'),
              url: 'https://weblate.framasoft.org/languages/'
            }
          ],
        },
        {
          title: v.$pgettext("Guide step title", 'Configure your languages'),
          content: [v.$pgettext("Guide step content", "Telling Weblate what languages you understand/translate will make it easier afterwards. You can also take a moment to edit your account profile and settings if needed."),],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'Configure your languages'),
              url: 'https://weblate.framasoft.org/accounts/profile/'
            }
          ],
        },
        {
          title: v.$pgettext("Guide step title", 'Click "Translate"'),
          content: [v.$pgettext("Guide step content", 'Click on the "translate" button in the last column of the languages table to start working.'),],
          media: [
            {
              type: 'image',
              url: 'assets/guides/translate/translate-button.png',
              caption: v.$pgettext("Task image caption", "The translate button is located in the last column")
            },
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Submit your first translation'),
          content: [v.$pgettext("Guide step content", "Weblate will now present you with text in need of translation or correction. Confidently submit a translation, or fill-in your proposal and submit the form. If you're not sure at all, comment or move to the next string."),],
        },
        {
          title: v.$pgettext("Guide step title", 'Stop here… or continue!'),
          content: [v.$pgettext("Guide step content", "You can repeat the previous step until you run out of time. Each new or improved translation makes the project better."),],
        },
      ]
    },
    {
      name: v.$pgettext('Task name', "Learn to edit OpenStreetMap"),
      icon: "map",
      slug: 'openstreetmap',
      tags: [ v.$pgettext('Task tag', 'OpenStreetMap') ],
      duration: 15,
      summary: v.$pgettext("Task summary", "Add details to the community-based world map"),
      equipments: [
        {
          name: v.$pgettext("Skill name", 'A mouse'),
          summary: v.$pgettext("Skill summary", "Editing the map is easier with a mouse in hand.")
        },
      ],
      steps: [
        {
          title: v.$pgettext("Guide step title", 'Head over to openstreetmap.org'),
          content: [
              v.$pgettext("Guide step content", 'Get used to the interface by try to view in detail your current location, or another location of your choice. You may also search a location by name in the search bar.')
          ],
          links: [
            {
              icon: 'map',
              text: v.$pgettext("Text for link in guide step", 'Access openstreetmap.org'),
              url: 'https://openstreetmap.org'
            },
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Register an account'),
          content: [
            v.$pgettext("Guide step content", "Modifications are only allowed to registered users, so let's click on \"Register\" on the top right of the screen and create an account.")
          ],
        },
        {
          title: v.$pgettext("Guide step title", 'Enter edit mode'),
          content: [
            v.$pgettext("Guide step content", "Once registered, let's enter edit mode to make your first modifications."),
            v.$pgettext("Guide step content", "After a few seconds, the edition interface should be shown"),
          ]
        }
      ]
    },
    {
      name: v.$pgettext('Task name', "Report a bug to Framasoft"),
      icon: "bug",
      slug: 'report-a-bug',
      tags: [ v.$pgettext('Task tag', 'Feedback') ],
      duration: 15,
      summary: v.$pgettext("Task summary", "Report typos, display issues, or other unexpected behaviour on one of Framasoft services or websites."),
      steps: [
        {
          title: v.$pgettext("Guide step title", 'Search for similar bugs'),
          content: [
            v.$pgettext("Guide step content", "A quick search can yield similar bug and avoid opening duplicates. Take a minute or two to search our issue tracker or our forum for recent bug reports that could match yours, but don't worry if you open a duplicate: a contributor will handle it :)"),
            v.$pgettext("Guide step content", "If you find a similar bug report, you can comment on this one instead of creating a new one."),
          ],
          links: [
            {
              icon: 'gitlab',
              text: v.$pgettext("Text for link in guide step", 'Browse existing bugs in our code repositories'),
              url: 'https://framagit.org/groups/framasoft/',
            },
            {
              icon: 'comment',
              text: v.$pgettext("Text for link in guide step", 'Check for bugs and/or start a thread on our support forum'),
              url: 'https://framacolibri.org/',
            },
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Create your Contributor Account'),
          content: [v.$pgettext("Guide step content", 'This account is needed to participate to threads in our forum.'),],
          links: [
            {
              icon: 'external-link',
              text: v.$pgettext("Text for link in guide step", 'Create your Framacolibri account'),
              url: 'https://framacolibri.org/',
            }
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Write your bug report'),
          content: [
            v.$pgettext("Guide step content", 'Open a new thread in the most appropriate category.'),
            v.$pgettext("Guide step content", 'Try to be as precise and specific as possible: the other contributors that will read your report may not know things that are obvious to you.'),
            v.$pgettext("Guide step content", "We know writing an actionable bug report is hard. Do your best and give as much context as possible, but don't worry: other contributors will reach out to you if they need more details."),
          ],
          recommendations: [
            v.$pgettext("Advice content / recommendations", "Explain what you were trying to do"),
            v.$pgettext("Advice content / recommendations", "Explain what you expected to happen"),
            v.$pgettext("Advice content / recommendations", "Explain what actually happened"),
            v.$pgettext("Advice content / recommendations", "Include logs, error messages, screenshots and information about your system (web browser, operating system…), if applicable"),
          ],
          avoid: [
            v.$pgettext("Advice content / avoid", 'Use vague language such as "this is not working"'),
          ],
        },
        {
          title: v.$pgettext("Guide step title", 'Review and submit your report'),
          content: [
            v.$pgettext("Guide step content", 'Read your report one last time, then click on the "Submit" button.'),
            v.$pgettext("Guide step content", 'A contributor will have a look at it within the next days!'),
          ],
        }
      ]
    },
    {
      name: v.$pgettext('Task name', "Review the documentation of Framasoft services"),
      icon: "search",
      duration: 15,
      tags: [ v.$pgettext("Task tag", 'Documentation') ],
      slug: 'review-documentation',
      summary: v.$pgettext("Task summary", "Ensure the documentation is accurate, understandable and up-to-date."),
      steps: [
        {
          title: v.$pgettext("Guide step title", "Head over to the documentation and pick a service"),
          content: [
            v.$pgettext("Guide step content", "You can start by picking a service you know well so that you don't need to get to know about the service and it's interface."),
            v.$pgettext("Guide step content", "Otherwise, just pick one at random and use it a bit to understand what it's all about.")
          ],
          links: [
            {
              icon: 'book',
              text: v.$pgettext('Text for link in guide step', 'View the documentation index'),
              url: 'https://docs.framasoft.org/'
            }
          ]
        },
        {
          title: v.$pgettext('Guide step title', 'Proofread the documentation carefully'),
          content: [
              v.$pgettext('Guide step content', 'You can check for the following issues:'),
          ],
          recommendations: [
            v.$pgettext("Advice content / recommendations", "Typos"),
            v.$pgettext("Advice content / recommendations", "Outdated instructions"),
            v.$pgettext("Advice content / recommendations", "Outdated screenshots"),
          ],
        },
        {
          title: v.$pgettext('Guide step title', 'Take note of each issue'),
          content: [
            v.$pgettext('Guide step content', "The nature of the issue and it's position"),
          ],
        },
        {
          title: v.$pgettext('Guide step title', 'Report the issues or fix them yourself'),
          content: [
            v.$pgettext('Guide step content', "Depending on how much time and how much motivation you have left, you can try to fix the issues yourself or report them"),
          ],
          links: [
            {
              icon: 'code',
              text: v.$pgettext('Text for link in guide step', 'Submit patches for the documentation'),
              url: 'https://participer.framasoft.org/fr/framacode/gitbook.html',
            },
            {
              icon: 'bug',
              text: v.$pgettext('Text for link in guide step', 'Report documentation issues'),
              url: 'https://framagit.org/framasoft/docs/issues/',
            }
          ]
        }
      ]
    },
    {
      name: v.$pgettext('Task name', "Support other Framasoft community members"),
      icon: "wrench",
      slug: "support-community-members",
      tags: [ v.$pgettext('Task tag', 'Support') ],
      duration: 30,
      summary: v.$pgettext("Task summary", "Help other community members troubleshot and solve their issues while installing or using Framasoft services."),
      skills: [
        {
          name: v.$pgettext('Skill name', 'Good interpersonal skills'),
          summary: v.$pgettext('Skill summary', "Sometime you'll need extra patience")
        },
        {
          name: v.$pgettext('Skill name', 'Knowledge of some projects'),
        }
      ],
      steps: [
        {
          title: v.$pgettext("Guide step title", "Register an account on Framacolibri"),
          content: [
              v.$pgettext("Guide step content", 'It will just take a second and is required to post on the Framacolibri forum.'),
          ],
        }
      ]
    },
    {
      name: v.$pgettext('Task name', "Write about a project you like"),
      icon: "pencil",
      duration: 60,
      tags: [ v.$pgettext('Task tag', 'Communication') ],
      summary: v.$pgettext("Task summary", "Do you own a blog or are you the media? Indemnify yourself by writing about a project you like to help it attain eyeballs."),
    },
    {
      name: v.$pgettext('Task name', "Solve a small issue"),
      icon: "file-code-o",
      tags: [ v.$pgettext('Task tag', 'Code') ],
      duration: 60,
      summary: v.$pgettext("Task summary", "Improve the project by fixing a reported bug or implementing a requested enhancement."),
    },
    {
      name: v.$pgettext('Task name', "Audit a project security"),
      icon: "user-secret",
      tags: [ v.$pgettext('Task tag', 'Code') ],
      duration: 120,
      summary: v.$pgettext("Task summary", "Investigate and report possible security issues, and help solve them before users are affected. Please don't do this on Framasoft's infrastructure but through installing the projects yourself."),
    },
    {
      name: v.$pgettext('Task name', 'Learn how to contribute to Wikipedia'),
      icon: 'wikipedia-w',
      tags: [ v.$pgettext('Task tag', 'Wikipedia') ],
      duration: 15,
      summary: v.$pgettext("Task summary", 'Wikipedia is useful to all of us, why not contribute to it ?')
    },
    {
      name: v.$pgettext('Task name', 'Create and improve Wikipedia articles about Women'),
      icon: 'venus',
      tags: [ v.$pgettext('Task tag', 'Wikipedia') ],
      duration: 60,
      summary: v.$pgettext("Task summary", "Women are under-represented on Wikipedia because of the Gender bias. Let's change that!")
    },
     {
      name: v.$pgettext('Task name', "Edit a Framalibre page"),
      icon: 'pencil',
      slug: 'framalibre-edit',
      tags: [ v.$pgettext('Task tag', 'Framalibre') ],
      duration: 5,
      summary: v.$pgettext("Task summary", "Correct an incorrect information in a Framalibre page."),
      equipments: [
        {
          name: v.$pgettext("Skill name", 'A mouse, a keyboard :-)'),
          summary: v.$pgettext("Skill summary", "You don't need more !")
        },
      ],
      steps: [
        {
          title: v.$pgettext("Guide step title", 'Create an account'),
          content: [
              v.$pgettext("Guide step content", 'If you do not already have one, you need an a Framalibre account and to be logged to edit a page.')
          ],
          links: [
            {
              icon: 'pencil',
              text: v.$pgettext("Text for link in guide step", 'How to create a Framalibre account'),
              url: 'https://participer.framasoft.org/fr/framalibre/se-creer-un-compte.html'
            },
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Edit a page'),
          content: [
            v.$pgettext("Guide step content", "If you spot an error in a page, click on the Edit button.")
          ],
            media: [
        {
          type: 'image',
          url: 'assets/guides/Framalibre-edit/framalibre-edit-button.png',
          caption: 'Edit Button on Framalibre',
        },
        ]
        },
        {
          title: v.$pgettext("Guide step title", 'Change the incorrect information'),
          content: [
            v.$pgettext("Guide step content", "Locate and change the incorrect information in the edit page."),
          ],
          media: [
        {
          type: 'image',
          url: 'assets/guides/Framalibre-edit/framalibre-edit-page.png',
          caption: 'Edit page on Framalibre',
        },
        ]
        },
        {
          title: v.$pgettext("Guide step title", 'Complete Revision history, preview the changes and save'),
          content: [
			v.$pgettext("Guide step content", "Complete the Revision history section at the bottom of the page, preview the changes and save."),
          ],
          media: [
        {
          type: 'image',
          url: 'assets/guides/Framalibre-edit/framalibre-edit-confirm.png',
          caption: 'Confirm button',
        },
        ]
        },
      ]
    },
    // hidden tasks (on home) that can be included in other guides
    {
      name: v.$pgettext('Task name', "Create a Framagit account"),
      icon: "gitlab",
      duration: 5,
      unlisted: true,
      slug: 'gitlab-account',
      summary: v.$pgettext("Task summary", 'Create your GitLab account on framagit.org to start working on various projects.'),
      steps: [
        {
          title: v.$pgettext("Guide step title", 'Visit framagit.org'),
          content: [v.$pgettext("Guide step content", 'The Framasoft GitLab instance is hosted at framagit.org. You have to sign-up here, using your e-mail account or by way of a third-party token provider.'),],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'Sign up on framagit.org'),
              url: 'https://framagit.org/users/sign_in',
            },
          ],
          media: [
            {
              type: 'image',
              url: 'assets/guides/gitlab-account/signup-form.png',
              caption: v.$pgettext("Task image caption", 'Sign-up form')
            },
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Verify your e-mail address'),
          content: [v.$pgettext("Guide step content", "Shortly after signup, you should receive a confirmation e-mail containing an activation link. Click it to activate your account. Request another one if you did not receive the e-mail within a few minutes."),],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'Request another activation e-mail'),
              url: 'https://framagit.org/users/almost_there',
            }
          ],
        },
        {
          title: v.$pgettext("Guide step title", 'Log in'),
          content: [v.$pgettext("Guide step content", "Log in using your new account to ensure everything works properly."),],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'Login form'),
              url: 'https://framagit.org/users/sign_in',
            }
          ],
          media: [
            {
              type: 'image',
              url: 'assets/guides/gitlab-account/login-form.png',
              caption: v.$pgettext("Task image caption", 'Login form')
            },
          ],
        },
        {
          title: v.$pgettext("Guide step title", 'Customize your profile'),
          optional: true,
          content: [v.$pgettext("Guide step content", "Take a few minutes to customize your profile and upload your avatar. This will help other community members know who you are :)"),],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'Profile form'),
              url: 'https://framagit.org/profile',
            }
          ],
          media: [
            {
              type: 'image',
              url: 'assets/guides/gitlab-account/user-dropdown.png',
              caption: v.$pgettext("Task image caption", 'Settings dropdown to access the profile form')
            },
            {
              type: 'image',
              url: 'assets/guides/gitlab-account/profile-form.png',
              caption: v.$pgettext("Task image caption", 'The profile form')
            },
          ],
        }
      ]
    },
    {
      name: v.$pgettext('Task name', "Create a Weblate account"),
      icon: "language",
      duration: 5,
      unlisted: true,
      slug: 'weblate-account',
      summary: v.$pgettext("Task summary", 'Create your Weblate account on weblate.framasoft.org to start translating various projects.'),
      steps: [
        {
          title: v.$pgettext("Guide step title", 'Visit weblate.framasoft.org'),
          content: [
              v.$pgettext("Guide step content", 'The Framasoft Weblate instance is hosted at weblate.framasoft.org. You have to sign-up here by using your e-mail account.'),
            v.$pgettext("Guide step content", 'Note : This email may be publicly accessible in the project history'),
          ],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'Sign up on weblate.framasoft.org'),
              url: 'https://weblate.framasoft.org/accounts/register/',
            },
          ],
          media: [
            {
              type: 'image',
              url: 'assets/guides/weblate-account/signup-form.png',
              caption: v.$pgettext("Task image caption", 'Sign-up form')
            },
          ]
        },
        {
          title: v.$pgettext("Guide step title", 'Verify your e-mail address'),
          content: [v.$pgettext("Guide step content", "Shortly after signup, you should receive a confirmation e-mail containing an activation link. Click it to activate your account. Request another one if you did not receive the e-mail within a few minutes."),],
        },
        {
          title: v.$pgettext("Guide step title", 'Log in'),
          content: [v.$pgettext("Guide step content", "Log in using your new account to ensure everything works properly."),],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'Login form'),
              url: 'https://weblate.framasoft.org/accounts/login/',
            }
          ],
        },
        {
          title: v.$pgettext("Guide step title", 'Customize your profile'),
          optional: true,
          content: [v.$pgettext("Guide step content", "Take a few minutes to customize your profile and upload your avatar. This will help other community members know who you are :)"),],
          links: [
            {
              text: v.$pgettext("Text for link in guide step", 'Profile form'),
              url: 'https://weblate.framasoft.org/accounts/profile/#account',
            }
          ],
        }
      ]
    },
  ]
}
